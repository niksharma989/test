###############################################################################
############ WEBLOGIC DOMAIN CREATION ACTIVITIES README FILE ##################
###############################################################################


# git Repo URL:
SSH:		git@gitlab.prod.fedex.com:3590978/wldca.git
https:		https://gitlab.prod.fedex.com/3590978/wldca.git


# Jenkins Job Link:
https://jenkins-ground.prod.cloud.fedex.com:8443/jenkins/job/RELEASE_MANAGEMENT/job/pr3590978/job/WebLogicDomainCreation/


# Dependencies
saxon-9.1.0.8.jar needs to be present in ant_home/lib


# General Project Information

SourceCode Details:

wldca
│   .classpath
│   .project
│   build.xml					---- Main Ant Build File
│
├───ExistingDomainDetails
│       1_MyDomain_DEV.xml		---- Maintain the config xml file for EXISTING DOMAIN 
|								---- This file will be automatically downloaded from SVN if its a request for an update to existing |							---- domain
│
├───Resources
│   ├───Config
│   │       Properties.properties	--- this property file has AppAccount user home directory & CM user name (cmbuild3)
│   │
│   ├───Templates				---- This directory holds the template files which are modified during actual installation process
│   │       .bash_profile		---- These are the files with place holders ({######}) which are replaced with actual values during build
│   │       bash_profile.txt
│   │       Footer.txt
│   │       Header.txt
│   │       install.sh
│   │
│   ├───XSD						----	These are the XSD files corresponding to AppInfoXML & AppConfig xml files
│   │       Schema_For_AppConfigXML.xsd
│   │       Schema_For_AppInfoXML.xsd
│   │
│   └───XSLT					----	These are the XSLTs used during xml transformation of AppInfo xml to AppConfig xml 
│           Xslt_ConfigToVersionConfig.xsl
│           Xslt_ForAppToConfig.xsl
│
├───Staging		---- This directory used as staging directory for temporary files during build phase
├───Target		----	This is the directory where all final scripts, property & configuration files are maintained after build
├───test		----	This is the directory to maintain some miscellaneous files which do not take part in build & install phase
│       AppInfo2.xml
│       AppInfo4.xml
│       Help.txt
│       Original_AppInfoxml
│
└───UserInput		----	This is the directory where AppInfo.xml file is expected when the Jenkins job or ANT target is 
        AppInfo.xml	----	called