# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
        . ~/.bashrc
fi

# User specific environment and startup programs
[ ! -e ~/logs ] && ln -s /var/fedex/{domainName}/logs/wls121/ logs
[ ! -e ~/dmm ] && ln -s /opt/fedex/{domainName}/stat/wls121{domainName} {domainName}
[ ! -e /var/fedex/{domainName}/logs/{domainName} ] && mkdir -p /var/fedex/{domainName}/logs/{domainName}

JAVA_HOME=/opt/java/hotspot/7/current
ANT_HOME=/opt/fedex/{domainName}/apache-ant-1.8.4
WL_HOME=/opt/weblogic/wl12.1.3.0/wlserver
PATH=$PATH:$ANT_HOME/bin:$JAVA_HOME/bin:$WL_HOME/common/bin

export PATH JAVA_HOME WL_HOME ANT_HOME
eval `perl -I ~/perl5/lib/perl5 -Mlocal::lib`
