<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
      <project>
         <xsl:if test="/Data/project[1]/@name">
            <xsl:attribute name="name">
               <xsl:value-of select="/Data/project[1]/@name"/>
            </xsl:attribute>
         </xsl:if>
         <xsl:copy-of select="/Data/project[1]/*"/>
         <version>
            <xsl:attribute name="number">
               <xsl:value-of select="concat(number(count(/Data/project[1]/version) +1),&quot;.0&quot;)"/>
            </xsl:attribute>
            <xsl:attribute name="author">
               <xsl:value-of select="&quot;SCM&quot;"/>
            </xsl:attribute>
         </version>
         <xsl:copy-of select="/Data/project[2]/cluster"/>
         <xsl:copy-of select="/Data/project[2]/server"/>
         <xsl:copy-of select="/Data/project[2]/dependency"/>
         <xsl:copy-of select="/Data/project[2]/dataSource"/>
      </project>
   </xsl:template>
</xsl:stylesheet>