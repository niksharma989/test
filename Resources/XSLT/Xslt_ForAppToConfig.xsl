<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<project>
			<xsl:attribute name="name">
            <xsl:value-of select="/WebLogicDomain/Domain" />
         </xsl:attribute>
			<version>
				<xsl:attribute name="number">
               <xsl:value-of select="1.0" />
            </xsl:attribute>
				<xsl:attribute name="author">
               <xsl:value-of select="&quot;SCM&quot;" />
            </xsl:attribute>
			</version>
			<xsl:for-each-group select="WebLogicDomain/ManagedServers/ManagedServer"
				group-by="ClusterName">
				<cluster>
					<xsl:attribute name="name">
                  <xsl:value-of select="ClusterName" />
               </xsl:attribute>
				</cluster>
			</xsl:for-each-group>
			<xsl:for-each-group select="/WebLogicDomain/ManagedServers/ManagedServer"
				group-by="ClusterName">
				<server>
					<xsl:attribute name="baseName">
                  <xsl:value-of select="substring(ClusterName,1,3)" />
               </xsl:attribute>
					<xsl:attribute name="nodes">
                  <xsl:value-of select="number(count(current-group()/Host))" />
               </xsl:attribute>
					<xsl:attribute name="cluster">
                  <xsl:value-of select="ClusterName" />
               </xsl:attribute>
					<xsl:attribute name="ListenPort">
                  <xsl:value-of select="Port" />
               </xsl:attribute>
					<xsl:choose>
						<xsl:when test="number(current-group()[1]/@id) =2">
							<xsl:attribute name="startnode">
                        <xsl:value-of
								select="number(current-group()[1]/@id) - 1" />
                     </xsl:attribute>
						</xsl:when>
						<xsl:otherwise>
							<xsl:if test="current-group()[1]/@id">
								<xsl:attribute name="startnode">
                           <xsl:value-of select="current-group()[1]/@id" />
                        </xsl:attribute>
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</server>
			</xsl:for-each-group>
		</project>
	</xsl:template>
</xsl:stylesheet>